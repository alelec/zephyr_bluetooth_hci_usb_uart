west build -p auto -b nrf52840dongle_nrf52840 -d ./build ./
nrfutil pkg generate --hw-version 52 --sd-req=0x00 --application build/zephyr/zephyr.hex --application-version 1 pkg.zip
echo "Program by plugging in dongle then hitting the right-andgle reset button to get red pulsing light"
echo "Check the usb serial port that comes up and program as such"
echo "nrfutil dfu serial -pkg pkg.zip -p COM14"