.. _bluetooth-hci-usb-uart:

Bluetooth: HCI UART over USB
############################

Overview
********

Make a Bluetooth UART HCI controller over USB-Serial connection with Zephyr. 
Requires USB device support from the board it runs on (e.g. :ref:`nrf52840dk_nrf52840` 
supports both BLE and USB).

Requirements
************

* Bluetooth stack running on the host (e.g. BlueZ)
* A board with Bluetooth and USB support in Zephyr

Building and Running
********************
Follow the zephyr getting started guides to set up the environment thwn point 
`west` at this directory. See `./build.sh` for example.

See :ref:`bluetooth samples section <bluetooth-samples>` for details.
